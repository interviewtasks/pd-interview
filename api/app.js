/**
 * If running outside of docker, fetch the environment variables manually
 */
if ('production' !== process.env.NODE_ENV) {
    require('dotenv').config({path: "../.env"})
}

const express = require("express");
const bodyParser = require('body-parser');
const OrganizationService = require('./service');
const service = new OrganizationService();
const app = express();

const port = process.env.API_PORT;

/**
 * configure the app to receive json requests
 */
app.use(bodyParser.json());

/**
 * Configure the API endpoints
 */
app.post('/organizations', async function (req, res) {
    let organizations = req.body;
    await service.storeOrganizations(organizations);
    res.status(200).json({status: "complete"});
});

app.get('/relatives/:organizationName/:page', async function (req, res) {
    let organizationName = req.params.organizationName;
    let page = req.params.page;
    let relatives = await service.getOrganizationRelatives(organizationName, page);
    res.status(200).json(relatives);
});

/**
 * Start up the application
 */
app.listen(port, () => {
    console.log(`Server is running at http://localohst:${port}/`);
});
