const DB = require('./repository');

module.exports = class OrganizationService{
    constructor(){
        this.db = new DB(
            process.env.MYSQL_HOST_ADDRESS,
            process.env.MYSQL_DATABASE,
            process.env.MYSQL_ROOT_USER,
            process.env.MYSQL_ROOT_PASSWORD
        );
    }

    async storeOrganizations(organizations){
        await this.db.addOrganization(organizations.org_name);
        await this.persistDaughterOrganizations(organizations.org_name, organizations.daughters);
    }

    async persistDaughterOrganizations(parent, daughters) {
        for (let daughter of daughters){
            await this.db.addOrganizationRelationship(parent, daughter.org_name);
            if(daughter.daughters){
                await this.persistDaughterOrganizations(daughter.org_name, daughter.daughters);
            }
        }
    }

    async getOrganizationRelatives(organizationName, page) {
        let allRelations = await this.db.getRelatedOrganizations(organizationName);

        let pageBounds = OrganizationService.getPageBounds(page, allRelations.length);

        let filteredRelations = [];
        for(let i = pageBounds.lowerBound; i <= pageBounds.upperBound && i < allRelations.length; i++){
            filteredRelations.push(allRelations[i]);
        }

        return filteredRelations;
    }

    static getPageBounds(page, lastItemIndex){
        const MAX_PAGE_SIZE = 100;
        if(page * MAX_PAGE_SIZE >= lastItemIndex){
            page = lastItemIndex - MAX_PAGE_SIZE - 1;
        }
        if(!page || page < 1){
            page = 1;
        }

        const upperBound = (page * MAX_PAGE_SIZE) - 1;
        const lowerBound = upperBound - MAX_PAGE_SIZE + 1;

        return {lowerBound: lowerBound, upperBound: upperBound};
    }
};